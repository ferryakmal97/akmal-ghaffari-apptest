import {NavigationContainer} from '@react-navigation/native';
import React from 'react';
import {Provider} from 'react-redux';
import FlashMessage from 'react-native-flash-message';

import {ProcessOverlay} from '~Components';
import Store from '~Redux';
import Routes from '~Routes';
import {navigationRef} from '~Utils';

const App = () => {
  return (
    <Provider store={Store}>
      <NavigationContainer ref={navigationRef}>
        <Routes />
      </NavigationContainer>
      <ProcessOverlay />
      <FlashMessage position="top" />
    </Provider>
  );
};

export default App;
