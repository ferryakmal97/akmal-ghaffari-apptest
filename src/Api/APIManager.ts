import axios, {AxiosError, AxiosResponse} from 'axios';

const APIManager = axios.create({
  baseURL: 'https://contact.herokuapp.com',
});

APIManager.interceptors.response.use(
  (response: AxiosResponse) => {
    return response;
  },
  (error: AxiosError) => {
    return Promise.reject(error);
  },
);

APIManager.defaults.headers.common['Content-Type'] = 'application/json';

export default APIManager;
