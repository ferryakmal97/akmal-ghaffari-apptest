import {useDispatch, useSelector} from 'react-redux';

import {AppDispatch, RootState} from '~Redux';
import {hideCustomLoader, showCustomLoader} from '~Redux/Loader';
import {Loader} from '~Types';

export default () => {
  const dispatch: AppDispatch = useDispatch();
  const {isVisible}: Loader = useSelector((state: RootState) => state.loader);

  function showProcessOverlay() {
    dispatch(showCustomLoader());
  }

  function hideProcessOverlay() {
    dispatch(hideCustomLoader());
  }

  return {
    isVisible,
    showProcessOverlay,
    hideProcessOverlay,
  };
};
