import {AxiosResponse} from 'axios';
import {useDispatch, useSelector} from 'react-redux';
import {showMessage, hideMessage} from 'react-native-flash-message';

import APIManager from '~Api/APIManager';
import {useLoader} from '~Hooks';
import {AppDispatch, RootState} from '~Redux';
import {
  setListContact,
  refreshContact,
  setCurrActiveContact,
  delContact,
  putContact,
} from '~Redux/Contact';
import {Contact, ContactForm, ContactState, GetContactResponse} from '~Types';
import {navigationRef} from '~Utils';

export default () => {
  const dispatch: AppDispatch = useDispatch();
  const {listContact, currActiveContact}: ContactState = useSelector(
    (state: RootState) => state.contact,
  );
  const {showProcessOverlay, hideProcessOverlay} = useLoader();

  const getContact = async () => {
    showProcessOverlay();
    try {
      const result: AxiosResponse<GetContactResponse> = await APIManager(
        '/contact',
        {
          method: 'GET',
        },
      );

      dispatch(setListContact(result?.data?.data));
    } catch (error) {
    } finally {
      hideProcessOverlay();
    }
  };

  const deleteContact = async (id: string = '') => {
    showProcessOverlay();
    try {
      await APIManager(`/contact/${id}`, {
        method: 'DELETE',
      });

      showMessage({
        message: 'Contact Successfully Deleted',
        type: 'success',
      });
      dispatch(delContact(id));
      getContact();
      navigationRef.goBack();
    } catch (error) {
      showMessage({
        message: 'Contact Unsuccessfully Deleted',
        type: 'danger',
      });
    } finally {
      hideProcessOverlay();
    }
  };

  const addContact = async (body: ContactForm) => {
    showProcessOverlay();

    try {
      await APIManager(`/contact`, {
        method: 'POST',
        data: body,
      });

      showMessage({
        message: 'Contact Successfully Added',
        type: 'success',
      });
      getContact();
      navigationRef.goBack();
    } catch (error) {
      showMessage({
        message: 'Contact Unsuccessfully Added',
        type: 'danger',
      });
    } finally {
      hideProcessOverlay();
    }
  };

  const editContact = async ({id, body}: {id: string; body: ContactForm}) => {
    showProcessOverlay();
    try {
      await APIManager(`/contact/${id}`, {
        method: 'PUT',
        data: body,
      });

      showMessage({
        message: 'Contact Successfully Edited',
        type: 'success',
      });
      dispatch(putContact({id, ...body}));
      getContact();
      navigationRef.goBack();
    } catch (error) {
      showMessage({
        message: 'Contact Unsuccessfully Edited',
        type: 'danger',
      });
    } finally {
      hideProcessOverlay();
    }
  };

  const selectCurrContact = (payload: Contact | null) => {
    dispatch(setCurrActiveContact(payload));
  };

  return {
    currActiveContact,
    listContact,
    getContact,
    selectCurrContact,
    deleteContact,
    addContact,
    editContact,
  };
};
