import {StyleSheet} from 'react-native';

import {Colors, normalize, SCREEN_WIDTH} from '~Utils';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.ORANGE,
    elevation: 3,
    width: '46%',
    borderRadius: normalize(10),
    marginLeft: normalize(10),
    marginTop: normalize(10),
  },

  imgBackgroundContainer: {
    width: '100%',
    aspectRatio: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  imgBackgroundstyle: {
    borderRadius: normalize(10),
  },

  image: {
    width: '50%',
    height: '50%',
    borderRadius: normalize(180),
  },
});
