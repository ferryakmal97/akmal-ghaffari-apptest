import {
  Pressable,
  StyleProp,
  PressableProps,
  ImageBackground,
  Image,
} from 'react-native';

import {Text} from '~Components';
import {useContact} from '~Hooks';
import {Contact} from '~Types';
import {Colors, imageFile, navigationRef} from '~Utils';

import {styles} from './styles';

interface ContactCardProps {
  style?: StyleProp<PressableProps>;
  data: Contact;
}

const ContactCard = ({style, data}: ContactCardProps) => {
  const {firstName, lastName, photo} = data || {};

  const {selectCurrContact} = useContact();

  const onCardPressHandler = () => {
    selectCurrContact(data);
    navigationRef.navigate('DetailScreen');
  };

  return (
    <Pressable style={[styles.container, style]} onPress={onCardPressHandler}>
      <ImageBackground
        source={imageFile(photo)}
        style={styles.imgBackgroundContainer}
        imageStyle={styles.imgBackgroundstyle}
        blurRadius={3}>
        <Image source={imageFile(photo)} style={styles.image} />
        <Text size="medium" weight="bold" color={Colors.WHITE}>
          {firstName}
        </Text>
        <Text size="small" weight="regular" color={Colors.WHITE}>
          {lastName}
        </Text>
      </ImageBackground>
    </Pressable>
  );
};

export default ContactCard;
