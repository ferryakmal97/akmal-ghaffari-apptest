import {Pressable} from 'react-native';

import {Text, Icons, Container, Gap} from '~Components';
import {Colors, normalize} from '~Utils';

interface CheckBoxProps {
  value: boolean;
  onCheck: () => void;
  label?: string;
}

const CheckBox = ({value, onCheck, label}: CheckBoxProps) => {
  return (
    <Pressable onPress={onCheck}>
      <Container type="row">
        <Icons name={value ? 'check-circle' : 'circle'} color={Colors.GREY} />
        <Gap width={normalize(5)} />
        {label && <Text color={Colors.BLACK}>{label}</Text>}
      </Container>
    </Pressable>
  );
};

export default CheckBox;
