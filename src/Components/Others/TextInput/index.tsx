import React, {ReactNode, useState} from 'react';
import {
  TextInput as RNTextInput,
  View,
  TouchableOpacity,
  TextInputProps,
  StyleProp,
  ViewStyle,
} from 'react-native';

import {Text, Icons} from '~Components';
import {Colors, normalize} from '~Utils';
import {styles} from './style';

interface CustomTextInputProps extends TextInputProps {
  containerStyle?: StyleProp<ViewStyle>;
  errMessage?: string;
  leftIcon?: ReactNode;
  isPhoneNum?: boolean;
  placeholderText: string;
  isPassword?: boolean;
  value: string;
}

const TextInput = ({
  value,
  onChangeText,
  placeholderText,
  style,
  containerStyle = {},
  keyboardType = 'default',
  isPassword = false,
  isPhoneNum = false,
  errMessage,
  leftIcon,
  ...rest
}: CustomTextInputProps) => {
  const [isShowSecureText, setIsShowSecureText] = useState(isPassword);

  // MARK: Events
  const onEyeIconPress = () => {
    setIsShowSecureText(!isShowSecureText);
  };

  // MARK: Renders methods
  const formStatus = () => {
    return errMessage ? (
      <View style={styles.errContainer}>
        <Text
          color={errMessage ? Colors.RED : Colors.BACKGROUND_BLACK_TRANSPARENT}
          size="small">
          {`* ${errMessage}`}
        </Text>
      </View>
    ) : (
      <></>
    );
  };

  return (
    <View style={[style, !errMessage && {marginBottom: normalize(12)}]}>
      <View style={[styles.defaultContainer, containerStyle]}>
        <View style={styles.defaultRow}>
          {leftIcon && <View style={styles.sectionLeftIcon}>{leftIcon}</View>}

          <RNTextInput
            value={value}
            onChangeText={onChangeText}
            style={[
              styles.defaultTextInput,
              {
                paddingLeft: leftIcon ? normalize(5) : normalize(10),
              },
            ]}
            keyboardType={isPhoneNum ? 'number-pad' : keyboardType}
            secureTextEntry={isShowSecureText}
            placeholder={placeholderText}
            placeholderTextColor={Colors.GREY}
            {...rest}
          />
          {isPassword && (
            <TouchableOpacity
              onPress={onEyeIconPress}
              style={styles.sectionPasswordShow}>
              <Icons
                name={isShowSecureText ? 'eye' : 'eye-slash'}
                type="fontawesome"
              />
            </TouchableOpacity>
          )}
        </View>
      </View>
      {formStatus()}
    </View>
  );
};

export default TextInput;
