import {StyleSheet} from 'react-native';

import {Colors, normalize} from '~Utils';

export const styles = StyleSheet.create({
  defaultContainer: {
    width: '100%',
    height: normalize(48),
    backgroundColor: Colors.WHITE,
    borderRadius: normalize(12),
    paddingLeft: normalize(5),
    alignSelf: 'center',
    elevation: 3,
  },
  defaultLabel: {
    fontWeight: 'bold',
    fontSize: normalize(20),
    marginTop: normalize(10),
  },
  defaultRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  sectionIconPhoneNum: {
    flex: 0.1,
    justifyContent: 'center',
  },
  iconPhoneNum: {
    width: normalize(26),
    height: normalize(26),
  },
  sectionPhoneNum: {
    flex: 0.2,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  iconDown: {
    width: normalize(10),
    height: normalize(10),
    marginLeft: normalize(6),
  },
  textPhoneNum: {
    fontWeight: 'bold',
    color: Colors.BLACK,
  },
  sectionPassword: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  sectionPasswordShow: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: normalize(8),
  },
  iconPassword: {
    width: normalize(26),
    height: normalize(26),
  },
  iconPasswordShow: {
    width: normalize(26),
    height: normalize(26),
  },
  defaultTextInput: {
    flex: 1,
    fontSize: normalize(16),
    paddingHorizontal: normalize(10),
    color: Colors.BLACK,
    height: normalize(40),
  },
  errContainer: {
    paddingVertical: normalize(5),
  },
  sectionLeftIcon: {
    flex: 0.1,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: normalize(5),
  },
});
