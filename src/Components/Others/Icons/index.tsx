import React from 'react';
import {StyleProp, ViewStyle} from 'react-native';

import Ant from 'react-native-vector-icons/AntDesign';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import IonIcons from 'react-native-vector-icons/Ionicons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import MaterialCommunity from 'react-native-vector-icons/MaterialCommunityIcons';
import Octicons from 'react-native-vector-icons/Octicons';
import SimpleLine from 'react-native-vector-icons/SimpleLineIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import {Colors, normalize} from '~Utils';

interface IconsProps {
  type?:
    | 'feather'
    | 'material'
    | 'octicon'
    | 'antdesign'
    | 'entypo'
    | 'ionicon'
    | 'material-community'
    | 'simple-line-icon'
    | 'fontawesome';
  name: string;
  size?: number;
  color?: string;
  style?: StyleProp<ViewStyle>;
}

const Icons = ({
  type = 'feather',
  name,
  size = normalize(20),
  color = Colors.WHITE,
  style = {},
}: IconsProps) => {
  if (name) {
    switch (type) {
      case 'material':
        return (
          <MaterialIcons name={name} size={size} color={color} style={style} />
        );

      case 'octicon':
        return <Octicons name={name} size={size} color={color} style={style} />;

      case 'antdesign':
        return <Ant name={name} size={size} color={color} style={style} />;

      case 'entypo':
        return <Entypo name={name} size={size} color={color} style={style} />;

      case 'ionicon':
        return <IonIcons name={name} size={size} color={color} style={style} />;

      case 'material-community':
        return (
          <MaterialCommunity
            name={name}
            size={size}
            color={color}
            style={style}
          />
        );

      case 'simple-line-icon':
        return (
          <SimpleLine name={name} size={size} color={color} style={style} />
        );

      case 'fontawesome':
        return (
          <FontAwesome name={name} size={size} color={color} style={style} />
        );

      default:
        return <Feather name={name} size={size} color={color} style={style} />;
    }
  }
  return null;
};

export default Icons;
