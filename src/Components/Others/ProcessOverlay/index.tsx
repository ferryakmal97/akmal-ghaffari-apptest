import React from 'react';
import {View, ActivityIndicator} from 'react-native';

import {useLoader} from '~Hooks';

import {styles} from './styles';

export default function ProcessOverlay() {
  const {isVisible} = useLoader();

  return isVisible ? (
    <View style={styles.blackoverlay}>
      <ActivityIndicator size="large" color="white" />
    </View>
  ) : null;
}
