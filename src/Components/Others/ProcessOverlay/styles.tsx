import {StyleSheet} from 'react-native';

import {Colors, SCREEN_HEIGHT, SCREEN_WIDTH} from '~Utils';

export const styles = StyleSheet.create({
  blackoverlay: {
    position: 'absolute',
    top: 0,
    left: 0,
    width: SCREEN_WIDTH,
    height: SCREEN_HEIGHT,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.BACKGROUND_BLACK_TRANSPARENT,
  },
});
