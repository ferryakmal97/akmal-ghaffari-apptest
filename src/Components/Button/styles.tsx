import {StyleSheet} from 'react-native';

import {Colors, normalize} from '~Utils';

export const styles = StyleSheet.create({
  button: {
    padding: normalize(10),
    borderRadius: normalize(10),
    marginVertical: normalize(5),
    elevation: 3,
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
    maxHeight: normalize(42),
  },
  primary: {
    backgroundColor: Colors.BLUE,
  },
  secondary: {
    backgroundColor: Colors.ORANGE,
  },
  clear: {
    backgroundColor: Colors.WHITE,
    borderWidth: 1,
    borderColor: Colors.BLACK,
  },
  danger: {
    backgroundColor: Colors.RED,
  },
  disabled: {
    backgroundColor: Colors.GREY,
  },
});
