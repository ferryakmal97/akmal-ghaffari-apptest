import {Pressable, StyleProp, PressableProps} from 'react-native';

import {Text} from '~Components';
import {Colors} from '~Utils';
import {styles} from './styles';

interface CustomButtonProps {
  children: string;
  onPress: () => void;
  type?: 'primary' | 'secondary' | 'clear' | 'danger';
  style?: StyleProp<PressableProps>;
}

export const Button = ({
  children,
  onPress,
  type = 'primary',
  style,
}: CustomButtonProps) => {
  return (
    <Pressable onPress={onPress} style={[styles.button, styles[type], style]}>
      <Text
        weight="semibold"
        color={type === 'clear' ? Colors.BLACK : Colors.WHITE}>
        {children}
      </Text>
    </Pressable>
  );
};
