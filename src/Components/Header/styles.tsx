import {StyleSheet} from 'react-native';
import {Colors, normalize} from '~Utils';

export const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: Colors.BLUE,
    padding: normalize(20),
    width: '100%',
  },
  mainContainerShadow: {
    elevation: 3,
    shadowOpacity: 0.1,
    shadowOffset: {
      width: 1,
      height: 3,
    },
    zIndex: 1,
  },
  rowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  backButton: {
    marginVertical: normalize(-10),
    marginLeft: normalize(-5),
  },
  title: {
    alignSelf: 'center',
    letterSpacing: 2,
  },
});
