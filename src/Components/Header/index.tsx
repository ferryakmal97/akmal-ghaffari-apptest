import React, {ReactNode} from 'react';
import {
  Pressable,
  View,
  StyleProp,
  ViewStyle,
  TextStyle,
  SafeAreaView,
} from 'react-native';

import {Icons, Text} from '~Components';
import {Colors, navigationRef, normalize} from '~Utils';

import {styles} from './styles';

interface HeaderProps {
  title: string;
  containerStyle?: StyleProp<ViewStyle>;
  titleStyle?: StyleProp<TextStyle>;
  children?: ReactNode;
  rightButton?: ReactNode;
  backButtonColor?: string;
  isShadow?: boolean;
  canGoBack?: boolean;
}

export const Header = ({
  title,
  containerStyle,
  titleStyle,
  children,
  rightButton,
  backButtonColor = Colors.WHITE,

  isShadow = true,
  canGoBack = true,
}: HeaderProps) => {
  const ableGoBack = canGoBack && navigationRef.canGoBack();

  return (
    <SafeAreaView
      style={[
        styles.mainContainer,
        containerStyle,
        isShadow && styles.mainContainerShadow,
      ]}>
      <View style={styles.rowContainer}>
        {ableGoBack && (
          <Pressable
            style={styles.backButton}
            onPress={() => navigationRef.goBack()}>
            <Icons
              type="material"
              name="arrow-back"
              size={normalize(30)}
              color={backButtonColor}
            />
          </Pressable>
        )}

        <View style={[styles.rowContainer, {flex: ableGoBack ? 1 : 0}]}>
          {children || (
            <Text
              weight="semibold"
              color={Colors.WHITE}
              style={[
                styles.title,
                {marginLeft: ableGoBack ? normalize(10) : 0},
                titleStyle,
              ]}>
              {title}
            </Text>
          )}
        </View>
        {rightButton && rightButton}
      </View>
    </SafeAreaView>
  );
};
