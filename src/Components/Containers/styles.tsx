import {StyleSheet} from 'react-native';

import {Colors, normalize} from '~Utils';

export const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: normalize(20),
    backgroundColor: Colors.WHITE,
  },

  box: {},

  center: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  row: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
});
