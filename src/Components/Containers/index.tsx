import {ReactNode} from 'react';
import {View, StyleProp, ViewStyle} from 'react-native';
import {styles} from './styles';

interface ContainerProps {
  children: ReactNode;
  type?: 'container' | 'center' | 'box' | 'row';
  style?: StyleProp<ViewStyle>;
}

export const Container = ({children, style, type = 'box'}: ContainerProps) => {
  return <View style={[styles[type], style]}>{children}</View>;
};
