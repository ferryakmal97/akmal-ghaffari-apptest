import {StyleSheet} from 'react-native';

import {normalize} from '~Utils';

export const styles = StyleSheet.create({
  small: {
    fontSize: normalize(14),
  },
  medium: {
    fontSize: normalize(16),
  },
  large: {
    fontSize: normalize(24),
  },
  semibold: {
    fontFamily: 'Montserrat-SemiBold',
  },
  bold: {
    fontFamily: 'Montserrat-Bold',
  },
  regular: {
    fontFamily: 'Montserrat-Regular',
  },
  thin: {
    fontFamily: 'Montserrat-Thin',
  },
  left: {
    textAlign: 'left',
  },
  center: {
    textAlign: 'center',
  },
  right: {
    textAlign: 'right',
  },
});
