import {StyleProp, TextStyle} from 'react-native';
import {Text as RNText} from 'react-native';

import {Colors} from '~Utils';
import {styles} from './styles';

interface TextProps {
  children: React.ReactNode;
  style?: StyleProp<TextStyle>;
  size?: 'small' | 'medium' | 'large';
  weight?: 'semibold' | 'bold' | 'regular' | 'thin';
  color?: string;
  align?: 'left' | 'center' | 'right';
}

export const Text = ({
  children,
  style,
  weight = 'regular',
  size = 'medium',
  color = Colors.BLACK,
  align = 'left',
}: TextProps) => {
  return (
    <RNText
      style={[
        style,
        styles[weight],
        styles[size],
        styles[align],
        {color: color},
      ]}>
      {children}
    </RNText>
  );
};
