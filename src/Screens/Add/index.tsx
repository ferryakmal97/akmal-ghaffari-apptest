import {useState} from 'react';
import {showMessage} from 'react-native-flash-message';

import {
  Button,
  CheckBox,
  Container,
  Gap,
  Header,
  Icons,
  TextInput,
} from '~Components';
import {useContact} from '~Hooks';
import {ContactForm} from '~Types';
import {Colors, normalize} from '~Utils';

const AddScreen = () => {
  const {addContact} = useContact();
  const [form, setForm] = useState<ContactForm>({
    firstName: '',
    lastName: '',
    age: '',
  });
  const [isMale, setIsMale] = useState<boolean>(true);

  const {age, firstName, lastName} = form;

  const handleChange = (
    type: 'firstName' | 'lastName' | 'age',
    value: string,
  ) => {
    setForm({
      ...form,
      [type]: value,
    });
  };

  const handleChangeGenderPressHandler = () => {
    setIsMale(!isMale);
  };

  const onSubmitPressHandler = () => {
    if (!age || !firstName || !lastName) {
      showMessage({
        message: 'Contact Unsuccessfully Added',
        description: "The fields shouldn't be empty",
        type: 'warning',
      });
      return;
    }

    addContact({
      age,
      firstName,
      lastName,
      photo: `https://xsgames.co/randomusers/avatar.php?g=${
        isMale ? 'male' : 'female'
      }`,
    });
  };

  return (
    <>
      <Header title="Add" />
      <Container type="container">
        <TextInput
          placeholderText={'First Name'}
          value={firstName}
          onChangeText={val => handleChange('firstName', val)}
          leftIcon={<Icons name="user" color={Colors.GREY} />}
        />
        <TextInput
          placeholderText={'Last Name'}
          value={lastName}
          onChangeText={val => handleChange('lastName', val)}
          leftIcon={<Icons name="user" color={Colors.GREY} />}
        />
        <TextInput
          placeholderText={'Age'}
          value={age}
          onChangeText={val => handleChange('age', val)}
          leftIcon={<Icons name="user" color={Colors.GREY} />}
          isPhoneNum
        />
        <Container type="row">
          <CheckBox
            value={isMale}
            onCheck={handleChangeGenderPressHandler}
            label="Male"
          />
          <CheckBox
            value={!isMale}
            onCheck={handleChangeGenderPressHandler}
            label="Female"
          />
        </Container>
        <Gap height={normalize(10)} />
        <Button onPress={onSubmitPressHandler}>Submit</Button>
      </Container>
    </>
  );
};

export default AddScreen;
