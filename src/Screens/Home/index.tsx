import React, {useEffect} from 'react';
import {FlatList, Pressable, RefreshControl} from 'react-native';

import {ContactCard, Header, Icons} from '~Components';
import {useContact, useLoader} from '~Hooks';
import {navigationRef} from '~Utils';
import {styles} from './styles';

const HomeScreen = ({}) => {
  const {getContact, listContact} = useContact();
  const {isVisible} = useLoader();

  useEffect(() => {
    getContact();
  }, []);

  // MARK : Events
  const onAddPressHandler = () => {
    navigationRef.navigate('AddScreen');
  };

  return (
    <FlatList
      data={listContact}
      renderItem={({item}) => <ContactCard data={item} />}
      keyExtractor={item => `${item.id}`}
      ListHeaderComponent={
        <Header
          canGoBack={false}
          title="Contact"
          rightButton={
            <Pressable onPress={onAddPressHandler}>
              <Icons name="person-add" type="material" />
            </Pressable>
          }
        />
      }
      numColumns={2}
      style={styles.container}
      contentContainerStyle={styles.contentContainer}
      refreshControl={
        <RefreshControl onRefresh={getContact} refreshing={isVisible} />
      }
    />
  );
};

export default HomeScreen;
