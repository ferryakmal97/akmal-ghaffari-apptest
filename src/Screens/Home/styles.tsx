import {StyleSheet} from 'react-native';

import {Colors, normalize} from '~Utils';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.WHITE,
  },

  contentContainer: {
    paddingBottom: normalize(20),
  },
});
