import AddScreen from './Add';
import DetailScreen from './Detail';
import EditScreen from './Edit';
import HomeScreen from './Home';

export {AddScreen, DetailScreen, EditScreen, HomeScreen};
