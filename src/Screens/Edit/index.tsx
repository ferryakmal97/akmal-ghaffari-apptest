import {useState} from 'react';
import {showMessage} from 'react-native-flash-message';

import {
  Button,
  CheckBox,
  Container,
  Gap,
  Header,
  Icons,
  TextInput,
} from '~Components';
import {useContact} from '~Hooks';
import {Contact} from '~Types';
import {Colors, normalize} from '~Utils';

const EditScreen = () => {
  const {editContact, currActiveContact} = useContact();
  const [form, setForm] = useState<Contact>(currActiveContact);
  const [isMale, setIsMale] = useState<boolean>(true);

  const {age, firstName, lastName, id} = form;

  const handleChange = (
    type: 'firstName' | 'lastName' | 'age',
    value: string,
  ) => {
    setForm({
      ...form,
      [type]: value,
    });
  };

  const handleChangeGenderPressHandler = () => {
    setIsMale(!isMale);
  };

  const onSubmitPressHandler = () => {
    if (!age || !firstName || !lastName) {
      showMessage({
        message: 'Contact Unsuccessfully Edited',
        description: "The fields shouldn't be empty",
        type: 'warning',
      });
      return;
    }

    editContact({
      id: id,
      body: {
        age,
        firstName,
        lastName,
        photo: `https://xsgames.co/randomusers/avatar.php?g=${
          isMale ? 'male' : 'female'
        }`,
      },
    });
  };

  return (
    <>
      <Header title="Edit" />
      <Container type="container">
        <TextInput
          placeholderText={'First Name'}
          value={firstName}
          onChangeText={val => handleChange('firstName', val)}
          leftIcon={<Icons name="user" color={Colors.GREY} />}
        />
        <TextInput
          placeholderText={'Last Name'}
          value={lastName}
          onChangeText={val => handleChange('lastName', val)}
          leftIcon={<Icons name="user" color={Colors.GREY} />}
        />
        <TextInput
          placeholderText={'Age'}
          value={`${age}`}
          onChangeText={val => handleChange('age', val)}
          leftIcon={<Icons name="user" color={Colors.GREY} />}
          isPhoneNum
        />
        <Container type="row">
          <CheckBox
            value={isMale}
            onCheck={handleChangeGenderPressHandler}
            label="Male"
          />
          <CheckBox
            value={!isMale}
            onCheck={handleChangeGenderPressHandler}
            label="Female"
          />
        </Container>
        <Gap height={normalize(10)} />
        <Button onPress={onSubmitPressHandler}>Submit</Button>
      </Container>
    </>
  );
};

export default EditScreen;
