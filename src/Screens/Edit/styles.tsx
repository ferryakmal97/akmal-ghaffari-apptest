import {StyleSheet} from 'react-native';

import {Colors, normalize} from '~Utils';

export const styles = StyleSheet.create({
  container: {
    padding: normalize(20),
    backgroundColor: Colors.BLUE,
  },
});
