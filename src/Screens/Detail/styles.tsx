import {StyleSheet} from 'react-native';

import {Colors, normalize, SCREEN_WIDTH} from '~Utils';

export const styles = StyleSheet.create({
  container: {
    padding: normalize(20),
    backgroundColor: Colors.BLUE,
  },

  ImageContainer: {
    backgroundColor: Colors.ORANGE,
    elevation: 3,
    width: '100%',
  },

  imgBackgroundContainer: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    aspectRatio: 1.5,
  },
  imgBackgroundstyle: {},

  image: {
    width: normalize(SCREEN_WIDTH / 2),
    borderRadius: normalize(SCREEN_WIDTH),
    aspectRatio: 1,
  },
});
