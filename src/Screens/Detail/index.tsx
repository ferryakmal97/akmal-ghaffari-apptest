import {useEffect} from 'react';
import {Image, ImageBackground} from 'react-native';

import {Button, Container, Gap, Header, Text} from '~Components';
import {useContact} from '~Hooks';
import {Colors, imageFile, navigationRef, normalize} from '~Utils';
import {styles} from './styles';

const DetailScreen = () => {
  const {currActiveContact, selectCurrContact, deleteContact} = useContact();
  const {photo, firstName, lastName, age, id} = currActiveContact || {};

  useEffect(() => {
    return () => {
      selectCurrContact(null);
    };
  }, []);

  const onDeletePressHandler = () => {
    deleteContact(id);
  };

  const onEditPressHandler = () => {
    navigationRef.navigate('EditScreen');
  };

  return (
    <>
      <Header title="Detail" />
      <ImageBackground
        source={imageFile(photo)}
        style={styles.imgBackgroundContainer}
        imageStyle={styles.imgBackgroundstyle}
        blurRadius={3}>
        <Image source={imageFile(photo)} style={styles.image} />
      </ImageBackground>
      <Container type="container">
        <Container type="row">
          <Container>
            <Text size="large" weight="bold" color={Colors.BLACK}>
              {firstName}
            </Text>
            <Text color={Colors.BLACK}>{lastName}</Text>
          </Container>
          <Container>
            <Text align="right" size="large" weight="bold" color={Colors.BLACK}>
              {age}
            </Text>
            <Text align="right" color={Colors.BLACK}>
              years old
            </Text>
          </Container>
        </Container>
        <Gap height={normalize(20)} />
        <Container type="row">
          <Button type="secondary" onPress={onEditPressHandler}>
            Edit
          </Button>
          <Gap width={normalize(10)} />
          <Button type="danger" onPress={onDeletePressHandler}>
            Delete
          </Button>
        </Container>
      </Container>
    </>
  );
};

export default DetailScreen;
