export const Colors = {
  GREEN: '#70D54B',
  PURPLE: '#822EA7',
  ORANGE: '#FF8500',
  BLUE: '#00A5DF',
  GREY: '#A1A1A1',
  RED: '#f44336',
  BACKGROUND_BLACK_TRANSPARENT: '#0005',
  WHITE: '#fff',
  BLACK: '#000',
};
