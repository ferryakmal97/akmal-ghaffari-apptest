import {Alert, Dimensions, PixelRatio} from 'react-native';
import {defaultUserIMG} from '~Assets/image';

export const {width: SCREEN_WIDTH, height: SCREEN_HEIGHT} =
  Dimensions.get('window');

// NOTE: Method to show Alert with only OK button for user acknowledgment
export const showAcknowledgmentAlert = (
  title: string,
  message: string,
  onOkButtonPressHandler = () => {},
) => {
  Alert.alert(title, message, [
    {
      text: 'Ok',
      onPress: onOkButtonPressHandler,
    },
  ]);
};

export const isObjectEmpty = (obj: object) => {
  if (!obj || (Object.keys(obj).length === 0 && obj.constructor === Object)) {
    return true;
  }
};

export const normalize = (size: number) => {
  const scale = SCREEN_WIDTH / 411; // Based on Nexus 5X scale
  const newSize = size * scale;

  return Math.round(PixelRatio.roundToNearestPixel(newSize));
};

export const imageFile = (file: string = '') => {
  let isValidURL = /^https?:\/\//i;

  if (isValidURL.test(file)) {
    return {uri: file};
  }

  return defaultUserIMG;
};
