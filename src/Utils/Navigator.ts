import {createNavigationContainerRef} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';

import {RootStackParamList} from '~Types';

export const Stack = createNativeStackNavigator();
export const navigationRef = createNavigationContainerRef<RootStackParamList>();
