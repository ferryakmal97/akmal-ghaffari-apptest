import {configureStore, combineReducers} from '@reduxjs/toolkit';

import loaderReducer from './Loader';
import contactReducer from './Contact';

const rootReducer = combineReducers({
  loader: loaderReducer,
  contact: contactReducer,
});

const Store = configureStore({
  reducer: rootReducer,
});

export type RootState = ReturnType<typeof Store.getState>;
export type AppDispatch = typeof Store.dispatch;
export default Store;
