import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {Contact, ContactState} from '~Types';

const initialState: ContactState = {
  listContact: [],
  currActiveContact: null,
};

const contactSlice = createSlice({
  name: 'contact',
  initialState,
  reducers: {
    setListContact: (state: ContactState, action: PayloadAction<Contact[]>) => {
      state.listContact = action.payload;
    },
    refreshContact: (state: ContactState) => {
      state.listContact = [];
    },
    delContact: (state: ContactState, action: PayloadAction<string>) => {
      state.listContact = state.listContact.filter(
        value => value.id !== action.payload,
      );
    },
    setCurrActiveContact: (
      state: ContactState,
      action: PayloadAction<Contact | null>,
    ) => {
      state.currActiveContact = action.payload;
    },
    putContact: (state: ContactState, action: PayloadAction<Contact>) => {
      state.listContact = state.listContact.map(value => {
        if (value.id === action.payload.id) {
          return {
            ...action.payload,
          };
        } else {
          return value;
        }
      });
      state.currActiveContact = action.payload;
    },
  },
});

export const {
  setListContact,
  refreshContact,
  setCurrActiveContact,
  delContact,
  putContact,
} = contactSlice.actions;
export default contactSlice.reducer;
