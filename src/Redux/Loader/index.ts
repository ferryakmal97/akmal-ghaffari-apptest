import {createSlice, Dispatch} from '@reduxjs/toolkit';
import {Loader} from '~Types';

const initialState: Loader = {
  isVisible: false,
};

const loaderSlice = createSlice({
  name: 'loader',
  initialState,
  reducers: {
    showLoader: (state: Loader) => {
      state.isVisible = true;
    },
    hideLoader: (state: Loader) => {
      state.isVisible = false;
    },
  },
});

const actions = loaderSlice.actions;

// Exported Actions
export const showCustomLoader = () => (dispatch: Dispatch) => {
  dispatch(actions.showLoader());
};

export const hideCustomLoader = () => (dispatch: Dispatch) => {
  dispatch(actions.hideLoader());
};

export default loaderSlice.reducer;
