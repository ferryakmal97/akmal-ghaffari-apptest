import React from 'react';

import {AddScreen, DetailScreen, EditScreen, HomeScreen} from '~Screens';
import {Stack} from '~Utils';

export default function MainStack() {
  return (
    <Stack.Navigator
      screenOptions={{headerShown: false, gestureEnabled: false}}
      initialRouteName="HomeScreen">
      <Stack.Screen name="AddScreen" component={AddScreen} />
      <Stack.Screen name="DetailScreen" component={DetailScreen} />
      <Stack.Screen name="EditScreen" component={EditScreen} />
      <Stack.Screen name="HomeScreen" component={HomeScreen} />
    </Stack.Navigator>
  );
}
