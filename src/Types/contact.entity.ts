export interface Contact {
  id: string;
  firstName: string;
  lastName: string;
  age: number | string;
  photo?: string;
}

export interface ContactState {
  listContact: Contact[];
  currActiveContact: Contact | null;
}

export interface GetContactResponse {
  message: string;
  data: Contact[];
}

export interface DeleteContactResponse {
  message: string;
  data: string;
}

export interface ContactForm {
  firstName: string;
  lastName: string;
  age: string;
  photo?: string;
}
